const { of, from, filter, max, reduce, range, count, distinct, tap, defaultIfEmpty, map } = require("rxjs");

let msg = '1Hell2o W3orld4';
let ob$ = from(msg);

ob$.pipe(
    filter(x => !(isNaN(x) || x === ' ')),
    reduce((acc, x) => acc + parseInt(x), 0),

).subscribe(
    x => console.log(x)
);

