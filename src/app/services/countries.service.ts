import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  private _countries: any;
  private _countriesArrayStr: string[] = [];
  countries$ = new BehaviorSubject<string[]>(this._countriesArrayStr);

  constructor(private http: HttpClient) { }

  getCountries() {
    const observer = {
      next: (data: any) => {
        this._countries = data;
        this._countries.forEach((country: any) => {
          this._countriesArrayStr.push(country.name.common);
        });
        this.countries$.next(this._countriesArrayStr);
      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        console.log('complete');
      }
    }
   this.http.get('https://restcountries.com/v3.1/all').subscribe(observer);
  }
}
