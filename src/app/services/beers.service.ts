import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Beer } from '../models/beer';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  beers: Beer[] = [];
  beers$ = new BehaviorSubject<Beer[]>(this.beers);

  constructor(private http: HttpClient) { }

  getBeers() {
    const observer = {
      next: (response: any) => {
        response.forEach((jsonBeer: any) => {
          this.beers.push(new Beer(jsonBeer));
        });
        this.beers$.next(this.beers);
      },
      error: (error: any) => {
        console.error(error);
      },
      complete: () => {
        console.log('Complete!');
      }
    };
    this.http.get('https://api.punkapi.com/v2/beers').subscribe(observer);
  }
}
