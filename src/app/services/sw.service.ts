import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SwService {

  private _people: any[] = [];
  private _planets: any[] = [];
  private _films: any[] = [];
  private _species: any[] = [];
  private _vehicles: any[] = [];
  private _starships: any[] = [];

  private _complete = false;
  complete$ = new BehaviorSubject<boolean>(this._complete);
  
  people$ = new BehaviorSubject<any[]>(this._people);
  planets$ = new BehaviorSubject<any[]>(this._planets);
  
  constructor(private http: HttpClient) { }

  getPeople(value?: string) {
    let url = value ? value : 'https://swapi.dev/api/people';
    const observer = {
      next: (data: any) => {
        this._people = [...this._people, ...data.results];
        this.people$.next(this._people);
        data.next !== null ? this.getPeople(data.next) : this._setComplete();
      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        
        console.log('complete');
      }
    }

    this.http.get(url).subscribe(observer);
  }

  private _setComplete() {
    this._complete = true;
    this.complete$.next(this._complete);
  }

  
}
