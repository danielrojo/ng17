import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export enum CalculatorState {
  Init,
  FirstNumber,
  SecondNumber,
  Result
}

@Injectable()
export class CalculatorService {

  private _currentState = CalculatorState.Init;
  private _firstNumber = 0;
  private _secondNumber = 0;
  private _result = 0;
  private _operator = '';
  private _display = '';

  display$ = new BehaviorSubject<string>(this._display);

  constructor() {
  
  }

  handleSymbol(value: string) {
    value === 'c' ? this._clearCalculator() : null;
    switch (this._currentState) {
      case CalculatorState.Init:
        break;
      case CalculatorState.FirstNumber:
        if (this._is_Operator(value)) {
          this._operator = value;
          this._currentState = CalculatorState.SecondNumber;
          this._display += value;
        }
        break;
      case CalculatorState.SecondNumber:
        if (value === '=') {
          this._calculate();
          this._currentState = CalculatorState.Result;
          this._display += value + this._result;
        }
        break;
      case CalculatorState.Result:
        if (this._is_Operator(value)) {
          this._firstNumber = this._result;
          this._operator = value;
          this._secondNumber = 0;
          this._result = 0;
          this._currentState = CalculatorState.SecondNumber;
          this._display += this._firstNumber + value;
        }
        break;
    }
    this.display$.next(this._display);
  }

  private _clearCalculator() {
    this._firstNumber = 0;
    this._secondNumber = 0;
    this._result = 0;
    this._operator = '';
    this._currentState = CalculatorState.Init;
    this._display = '';
  }

  private readonly _is_Operator = (value: string) => value === '+' || value === '-' || value === '*' || value === '/';

  private _calculate() {
    switch (this._operator) {
      case '+':
        this._result = this._firstNumber + this._secondNumber;
        break;
      case '-':
        this._result = this._firstNumber - this._secondNumber;
        break;
      case '*':
        this._result = this._firstNumber * this._secondNumber;
        break;
      case '/':
        this._result = this._firstNumber / this._secondNumber;
        break;
    }

  }

  handleNumber(value: number) {
    switch (this._currentState) {
      case CalculatorState.Init:
        this._firstNumber = value;
        this._currentState = CalculatorState.FirstNumber;
        this._display = value.toString();
        break;
      case CalculatorState.FirstNumber:
        this._firstNumber = this._firstNumber * 10 + value;
        this._display += value.toString();
        break;
      case CalculatorState.SecondNumber:
        this._secondNumber = this._secondNumber * 10 + value;
        this._display += value.toString();
        break;
      case CalculatorState.Result:
        this._firstNumber = value;
        this._secondNumber = 0;
        this._result = 0;
        this._operator = '';
        this._currentState = CalculatorState.FirstNumber;
        this._display = value.toString();
        break;
    }
    this.display$.next(this._display);

  }

}
