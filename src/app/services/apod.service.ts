import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Apod } from '../models/apod';
import { ErrorApod } from '../models/error.apod';

@Injectable({
  providedIn: 'root'
})
export class ApodService {

  private _apod: Apod = new Apod();
  apod$ = new BehaviorSubject<Apod | ErrorApod>(this._apod);

  constructor(private http: HttpClient) { }

  getApod(stringDate?: string): void {
    const baseUrl = 'https://api.nasa.gov/planetary/apod';
    const key = 'DEMO_KEY';
    let url = '';
    let observer = {
      next: (data: any) => {
        this._apod = new Apod(data);
        this.apod$.next(this._apod);
      },
      error: (error: any) => {
        this.apod$.next(new ErrorApod(error));
        console.log(error);
      },
      complete: () => {
        console.log('complete');
      }
    }
    url = `${baseUrl}?api_key=${key}`;
    stringDate ? url = `${url}&date=${stringDate}` : url = url;
    this.http.get(url).subscribe(observer);
  }

}
