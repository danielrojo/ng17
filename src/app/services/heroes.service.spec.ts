import { TestBed } from '@angular/core/testing';

import { HeroesService } from './heroes.service';
import { Hero } from '../models/hero';

describe('HeroesService', () => {
  let service: HeroesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeroesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add 1 new valid hero', () => {
    const initialHeroes = service.heroes.length;
    const newHero = new Hero('Test Hero', 'Test Description');
    service.addHero(newHero);
    expect(service.heroes.length).toBe(initialHeroes + 1);
    expect(service.heroes[initialHeroes]).toEqual(newHero);
  });

  it('should remove last hero', () => {
    const initialHeroes = service.heroes.length;
    service.removeHero(initialHeroes - 1);
    expect(service.heroes.length).toBe(initialHeroes - 1);
  });

  it('should not remove heroes when index is negative', () => {
    const initialHeroes = service.heroes.length;
    service.removeHero(-1);
    expect(service.heroes.length).toBe(initialHeroes);
  });

  it('should not remove heroes when index is out of bounds', () => {
    const initialHeroes = service.heroes.length;
    service.removeHero(initialHeroes);
    expect(service.heroes.length).toBe(initialHeroes);
  });
});
