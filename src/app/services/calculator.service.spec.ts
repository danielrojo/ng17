import { TestBed } from '@angular/core/testing';
import { CalculatorService } from './calculator.service';

describe('CalculatorService', () => {
  let service: CalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculatorService],
    });
    service = TestBed.inject(CalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('1+2= should return 1+2=3', () => {
    let result = '';
    service.display$.subscribe((value) => {
      result = value;
    });
    service.handleNumber(1);
    expect(result).toBe('1');
    service.handleSymbol('+');
    expect(result).toBe('1+');
    service.handleNumber(2);
    expect(result).toBe('1+2');
    service.handleSymbol('=');
    expect(result).toBe('1+2=3');
  });

});
