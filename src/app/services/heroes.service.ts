import { Injectable } from '@angular/core';
import { Hero } from '../models/hero';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private _heroes: Hero[] = [new Hero('Spiderman', 'The amazing Spiderman'), new Hero('Batman', 'The dark knight')]

  constructor() { }

  get heroes() {
    return this._heroes;
  }

  addHero(hero: Hero) {
    if (hero) {
      this.heroes.push(hero);
    }
  }

  removeHero(index: number) {
    if (index >= 0 && index < this.heroes.length)
      this.heroes.splice(index, 1);
  }
}
