import { CommonModule, JsonPipe, NgFor } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { Hero } from '../../../models/hero';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [NgFor, NgbAlertModule, JsonPipe],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})
export class ListComponent {

  @Input() heroes: Hero[] = [];
  @Output() onRemoveHero = new EventEmitter<number>();

  removeHero(index: number) {
    this.onRemoveHero.emit(index);
  }
}
