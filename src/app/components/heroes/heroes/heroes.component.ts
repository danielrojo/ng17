import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListComponent } from '../list/list.component';
import { FormComponent } from '../form/form.component';
import { ApodService } from '../../../services/apod.service';
import { HttpClientModule } from '@angular/common/http';
import { Hero } from '../../../models/hero';
import { HeroesService } from '../../../services/heroes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroes',
  standalone: true,
  imports: [FormsModule, ListComponent, FormComponent],
  templateUrl: './heroes.component.html',
  styleUrl: './heroes.component.scss'
})
export class HeroesComponent implements OnInit{

  constructor(private service: HeroesService) { 
    this.heroes = service.heroes;
  }

  ngOnInit(): void {
    // this.route.paramMap.subscribe((params) => {
    //   const name = params.get('name');
    //   const description = params.get('description');
    //   if (name && description) {
    //     this.addHero(new Hero(name, description));
    //   }
    // });
  }

  heroes: Hero[] = [];

  addHero(hero: Hero) {
    this.service.addHero(hero);
  }

  removeHero(index: number) {
    this.service.removeHero(index);
  }


}
