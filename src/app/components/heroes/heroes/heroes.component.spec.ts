import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesComponent } from './heroes.component';
import { ActivatedRoute, RouterModule, RouterOutlet, provideRouter } from '@angular/router';
import { HeroesService } from '../../../services/heroes.service';
import { FormsModule } from '@angular/forms';
import { ListComponent } from '../list/list.component';
import { FormComponent } from '../form/form.component';
import { routes } from '../../../app.routes';

describe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterOutlet, RouterModule, HeroesComponent, FormsModule, ListComponent, FormComponent],
      providers: [HeroesService],
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
