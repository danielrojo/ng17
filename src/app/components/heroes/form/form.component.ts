import { NgIf } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Hero } from '../../../models/hero';

@Component({
  selector: 'app-form',
  standalone: true,
  imports: [FormsModule, NgIf],
  templateUrl: './form.component.html',
  styleUrl: './form.component.scss'
})
export class FormComponent {

  @Output() onAddHero = new EventEmitter<Hero>();
  newHeroName = '';
  newHeroDescription = '';


  addHero() {
    if (this.newHeroName !== '') {
      this.onAddHero.emit(new Hero(this.newHeroName, this.newHeroDescription));
      this.newHeroName = '';
      this.newHeroDescription = '';
    }
  }


}
