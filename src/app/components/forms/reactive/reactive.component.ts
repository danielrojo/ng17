import { Component, OnInit } from '@angular/core';
import { Validators, FormArray, FormBuilder, ReactiveFormsModule, FormControl, AbstractControl, NgModel, FormsModule } from '@angular/forms';
import { CountriesService } from '../../../services/countries.service';
import { NgbTypeahead, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { OperatorFunction, Observable, debounceTime, distinctUntilChanged, map } from 'rxjs';

function validateName(control: AbstractControl) {
  // if first letter is not uppercase return error
  let result: any = null;
  control.value.charAt(0) !== control.value.charAt(0).toUpperCase() ? result = { invalidName: true } : result = null;
  return result;
}

@Component({
  selector: 'app-reactive',
  standalone: true,
  imports: [ReactiveFormsModule,FormsModule, NgbTypeaheadModule],
  templateUrl: './reactive.component.html',
  styleUrl: './reactive.component.scss'
})
export class ReactiveComponent implements OnInit{
  // regex of letters and spaces [3, 10]

  nameRgx = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,10}$/;
  countries: string[] = [];
  countryName = '';


  profileForm = this.fb.group({
    name: ['', [Validators.required, validateName]],
    description: ['']
  });

	search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
		text$.pipe(
			debounceTime(200),
			distinctUntilChanged(),
			map((term) =>
				term.length < 2 ? [] : this.countries.filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10),
			),
		);
  

  constructor(private fb: FormBuilder, private service: CountriesService) { }
  ngOnInit(): void {
    this.service.countries$.subscribe((countries) => {
      this.countries = countries;
      console.log(this.countries);
      
    }
    );
    this.service.getCountries();
  }

  updateProfile() {
    this.profileForm.patchValue({
      
    });
  }

  customValidator() {
    return (control: FormControl) => {
      const value = control.value;
      if (!value) {
        return null;
      }
      return value.charAt(0) === 'a' ? true : null;
    };
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }
}
