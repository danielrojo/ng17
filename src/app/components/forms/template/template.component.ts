import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Hero } from '../../../models/hero';

@Component({
  selector: 'app-template',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './template.component.html',
  styleUrl: './template.component.scss'
})
export class TemplateComponent {

  // regex of letters and spaces [3, 10]
  nameRgx = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/;

  model = new Hero('Chuck Overstreet', 'Description of Chuck Overstreet');

  submitted = false;

  onSubmit() { 
    this.submitted = true; 
    console.log(JSON.stringify(this.model));
    
  }

  newHero() {
    this.model = new Hero('');
  }


}
