import { Component, OnInit } from '@angular/core';
import { BeersService } from '../../../services/beers.service';
import { Beer } from '../../../models/beer';
import { NgxSliderModule, Options } from 'ngx-slider-v2';
import { filter, from, of, tap } from 'rxjs';
import { AsyncPipe, CurrencyPipe, JsonPipe, UpperCasePipe } from '@angular/common';
import { AbvPipe } from '../../../pipes/abv.pipe';

enum Order {
  AbvAsc,
  AbvDesc,
  Alphabetical,
}

@Component({
  selector: 'app-beers',
  standalone: true,
  imports: [NgxSliderModule, AbvPipe, UpperCasePipe, CurrencyPipe],
  templateUrl: './beers.component.html',
  styleUrl: './beers.component.scss'
})
export class BeersComponent implements OnInit{

  order = Order;

  beers: Beer[] = [];
  filteredBeers: Beer[] = [];


  classes: string[] = [
    'btn btn-secondary',
    'btn btn-secondary',
    'btn btn-secondary'
  ];

  lowestAbv: number = 4;
  highestAbv: number = 6;
  options: Options = {
    floor: 0,
    ceil: 60,
    step: 0.1,
  };

  constructor(private service: BeersService) { }

  ngOnInit(): void {
    this.service.beers$.subscribe((beers) => {
      this.beers = beers;
      this.filterBeers(this.lowestAbv, this.highestAbv);

    });
    this.service.getBeers();
  }

  orderBeers(order: Order): void {
    this.clearClasses();
    switch (order) {
      case Order.AbvAsc:
        this.beers.sort((a, b) => a.abv - b.abv);
        this.classes[0] = 'btn btn-primary';
        break;
      case Order.AbvDesc:
        this.beers.sort((a, b) => b.abv - a.abv);
        this.classes[1] = 'btn btn-primary';
        break;
      case Order.Alphabetical:
        this.beers.sort((a, b) => a.name.localeCompare(b.name));
        this.classes[2] = 'btn btn-primary';
        break;
    }
  }

  getClass(index: number): string {
    console.log('getClass: ' + index);
    
    return this.classes[index];
  }

  clearClasses() {
    this.classes = [
      'btn btn-secondary',
      'btn btn-secondary',
      'btn btn-secondary'
    ];
  }

  filterBeers(lowestAbv: number, highestAbv: number) {
    this.filteredBeers = [];
    console.log('filteredBeers: ' + lowestAbv + ' ' + highestAbv);
    from(this.beers).pipe(
      filter((beer: Beer) => beer.abv >= lowestAbv && beer.abv <= highestAbv),
      // tap((beer: any) => console.log('beer: ' + JSON.stringify(beer)))
    ).subscribe((beers) => {
      console.log('beers: ' + JSON.stringify(beers));
      this.filteredBeers = [...this.filteredBeers, beers]
      // this.filteredBeers = beers;
      
    });
  }

  handleChange(event: any) {
    console.log('handleChange: ' + JSON.stringify(event));
    
    this.filterBeers(event.value, event.highValue);
    // this.lowestAbv = event.value;
  }

}
