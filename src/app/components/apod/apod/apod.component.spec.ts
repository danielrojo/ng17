import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApodComponent } from './apod.component';
import { JsonPipe } from '@angular/common';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ApodDatePickerComponent } from '../apod-date-picker/apod-date-picker.component';
import { ApodInfoComponent } from '../apod-info/apod-info.component';
import { ApodService } from '../../../services/apod.service';

describe('ApodComponent', () => {
  let component: ApodComponent;
  let fixture: ComponentFixture<ApodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ApodComponent, HttpClientModule, FormsModule, JsonPipe, ApodInfoComponent, ApodDatePickerComponent],
      providers: [ApodService, HttpClient, HttpHandler],
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ApodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
