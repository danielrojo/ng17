import { Component, OnInit } from '@angular/core';
import { ApodService } from '../../../services/apod.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
import { Apod } from '../../../models/apod';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { YouTubePlayer } from '@angular/youtube-player';
import { ErrorApod } from '../../../models/error.apod';
import { ApodInfoComponent } from '../apod-info/apod-info.component';
import { ApodDatePickerComponent } from '../apod-date-picker/apod-date-picker.component';

@Component({
  selector: 'app-apod',
  standalone: true,
  imports: [HttpClientModule, FormsModule, JsonPipe, ApodInfoComponent, ApodDatePickerComponent],
  templateUrl: './apod.component.html',
  styleUrl: './apod.component.scss'
})
export class ApodComponent implements OnInit{

  private _response: Apod | ErrorApod = new Apod();
  // today date in format YYYY-MM-DD
  dateStr: string = new Date().toISOString().slice(0, 10);

  constructor() { 
    
  }

  ngOnInit(): void {
  }

  dateSelect(dateStr: string) {
    // get apod for date with format YYYY-MM-DD
    console.log(dateStr);
    
    this.dateStr = dateStr;
  }

}
