import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { YouTubePlayer } from '@angular/youtube-player';
import { Apod } from '../../../models/apod';
import { ErrorApod } from '../../../models/error.apod';
import { ApodService } from '../../../services/apod.service';

@Component({
  selector: 'app-apod-info',
  standalone: true,
  imports: [YouTubePlayer],
  templateUrl: './apod-info.component.html',
  styleUrl: './apod-info.component.scss'
})
export class ApodInfoComponent implements OnInit, OnChanges {

  // today date in format YYYY-MM-DD
  @Input() date: string = new Date().toISOString().slice(0, 10);
  apod: Apod | undefined;
  error: ErrorApod | undefined;

  constructor(private service: ApodService) { }


  ngOnInit(): void {
    this.service.apod$.subscribe((response) => {
      if (response instanceof Apod) {
        this.apod = response;
        this.error = undefined;
      } else {
        this.error = response;
        this.apod = undefined;
      }
    }
    );

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(JSON.stringify(changes));
    changes['date'].currentValue !== changes['date'].previousValue ? this.service.getApod(changes['date'].currentValue) : null;
  }

}
