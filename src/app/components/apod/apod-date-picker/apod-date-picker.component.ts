import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-apod-date-picker',
  standalone: true,
  imports: [NgbDatepickerModule, FormsModule],
  templateUrl: './apod-date-picker.component.html',
  styleUrl: './apod-date-picker.component.scss'
})
export class ApodDatePickerComponent {

  @Output() onDaySelect = new EventEmitter<string>();

  dateSelected: any = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};

  constructor() { }

  dateSelect() {
    this.onDaySelect.emit(`${this.dateSelected.year}-${this.dateSelected.month}-${this.dateSelected.day}`);
  }
}
