import { Component, OnInit, TemplateRef, inject } from '@angular/core';
import { SwService } from '../../../services/sw.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sw',
  standalone: true,
  imports: [],
  templateUrl: './sw.component.html',
  styleUrl: './sw.component.scss'
})
export class SwComponent implements OnInit{

  people: any[] = [];
  complete = false;
  selectedPerson: any = {};
  private modalService = inject(NgbModal);
	closeResult = '';

  constructor(private service: SwService) { }

  ngOnInit(): void {
    this.service.people$.subscribe((people) => {
      this.people = people;
      
    });

    this.service.complete$.subscribe((complete) => {
      this.complete = complete;
    });

    this.service.getPeople();
  }

  open(content: TemplateRef<any>, person: any) {
    this.selectedPerson = person;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

	private getDismissReason(reason: any): string {
		switch (reason) {
			case ModalDismissReasons.ESC:
				return 'by pressing ESC';
			case ModalDismissReasons.BACKDROP_CLICK:
				return 'by clicking on a backdrop';
			default:
				return `with: ${reason}`;
		}
	}

}
