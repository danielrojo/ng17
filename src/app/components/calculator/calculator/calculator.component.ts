import { Component, OnDestroy, OnInit } from '@angular/core';
import { CalculatorStateMachine } from '../../../models/calculator.state.machine';
import { CalculatorService } from '../../../services/calculator.service';
import { DisplayComponent } from '../display/display.component';
import { KeyboardComponent } from '../keyboard/keyboard.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calculator',
  standalone: true,
  imports: [DisplayComponent, KeyboardComponent],
  providers: [CalculatorService],
  templateUrl: './calculator.component.html',
  styleUrl: './calculator.component.scss'
})
export class CalculatorComponent implements OnInit, OnDestroy {

  display = '';

  constructor(private sm: CalculatorService, private route: Router) {

  }

  ngOnInit(): void {
    this.sm.display$.subscribe((display) => {
      this.display = display;
    });
  }

  ngOnDestroy(): void {
    console.log('CalculatorComponent destroyed');
    
  }

  handleButtonClick(value: string | number) {
    typeof value === 'string' ? this.sm.handleSymbol(value) : this.sm.handleNumber(value);
  }

  goToHeroes() {
    this.route.navigate(['/heroes', { name: 'Batman', description: 'The Dark Knight' }]);
  }



}
