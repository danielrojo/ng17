export class Beer {
    private _id = -1;
    private _name = '';
    private _tagline = '';
    private _description = '';
    private _image = '';
    private _abv = -1;

    constructor(jsonBeer?: any) {
        if (jsonBeer) {
            this._id = jsonBeer.id;
            this._name = jsonBeer.name;
            this._tagline = jsonBeer.tagline;
            this._description = jsonBeer.description;
            this._image = jsonBeer.image_url;
            this._abv = jsonBeer.abv;    
        }
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    get tagline() {
        return this._tagline;
    }

    get description() {
        return this._description;
    }

    get image() {
        return this._image;
    }

    get abv() {
        return this._abv;
    }

}