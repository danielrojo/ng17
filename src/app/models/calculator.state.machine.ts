enum CalculatorState {
    Init,
    FirstNumber,
    SecondNumber,
    Result
}

export class CalculatorStateMachine {
    currentState = CalculatorState.Init;
    firstNumber = 0;
    secondNumber = 0;
    result = 0;
    operator = '';

    display = '';

    handleSymbol(value: string) {
        value === 'c' ? this.clearCalculator() : null;
        switch (this.currentState) {
          case CalculatorState.Init:
            break;
          case CalculatorState.FirstNumber:
            if (this.isOperator(value)) {
              this.operator = value;
              this.currentState = CalculatorState.SecondNumber;
              this.display += value;
            }
            break;
          case CalculatorState.SecondNumber:
            if (value === '=') {
              this.calculate();
              this.currentState = CalculatorState.Result;
              this.display += value + this.result;
            }
            break;
          case CalculatorState.Result:
            if (this.isOperator(value)) {
              this.firstNumber = this.result;
              this.operator = value;
              this.secondNumber = 0;
              this.result = 0;
              this.currentState = CalculatorState.SecondNumber;
              this.display += this.firstNumber + value;
            }
            break;
        }
      }
    
      clearCalculator() {
        this.firstNumber = 0;
        this.secondNumber = 0;
        this.result = 0;
        this.operator = '';
        this.currentState = CalculatorState.Init;
        this.display = '';
      }
    
      readonly isOperator = (value: string) => value === '+' || value === '-' || value === '*' || value === '/';
    
      calculate() {
        switch (this.operator) {
          case '+':
            this.result = this.firstNumber + this.secondNumber;
            break;
          case '-':
            this.result = this.firstNumber - this.secondNumber;
            break;
          case '*':
            this.result = this.firstNumber * this.secondNumber;
            break;
          case '/':
            this.result = this.firstNumber / this.secondNumber;
            break;
        }
    
      }
    
      handleNumber(value: number) {
        switch (this.currentState) {
          case CalculatorState.Init:
            this.firstNumber = value;
            this.currentState = CalculatorState.FirstNumber;
            this.display = value.toString();
            break;
          case CalculatorState.FirstNumber:
            this.firstNumber = this.firstNumber * 10 + value;
            this.display += value.toString();
            break;
          case CalculatorState.SecondNumber:
            this.secondNumber = this.secondNumber * 10 + value;
            this.display += value.toString();
            break;
          case CalculatorState.Result:
            this.firstNumber = value;
            this.secondNumber = 0;
            this.result = 0;
            this.operator = '';
            this.currentState = CalculatorState.FirstNumber;
            this.display = value.toString();
            break;
        }
    
      }
    
}