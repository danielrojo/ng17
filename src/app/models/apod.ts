export class Apod {

    private _title = ''
    private _explanation = '';
    private _url = '';
    private _date = '';
    private _mediaType = '';
    private _hdUrl = '';
    private _isImage = false;
    private _isYoutubeVideo = false;
    private _youtubeVideoId = '';

    constructor(apod?: any) {
        if(apod) {
            this._title = apod.title;
            this._explanation = apod.explanation;
            this._url = apod.url;
            this._date = apod.date;
            this._mediaType = apod.media_type;
            apod.hdurl? this._hdUrl = apod.hdurl : this._hdUrl = '';
            this._isImage = apod.media_type === 'image'? true : false;
            this._isYoutubeVideo = apod.media_type === 'video' && this.youtubeParser(this._url) !== false? true : false;
            this._youtubeVideoId = this._getID(this._url);
        }
    }

    _getID(url: string): string {
        let id: string | boolean = '';
        let response = '';
        id = this.youtubeParser(url);
        id !== false? response = id.toString() : response = '';
        console.log(response);
        
        return response;
    }

    youtubeParser(url: string): string | boolean{
        const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
        let match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    get youtubeVideoId() {
        return this._youtubeVideoId;
    }

    get isImage() {
        return this._isImage;
    }

    get isYoutubeVideo() {
        return this._isYoutubeVideo;
    }

    get title() {
        return this._title;
    }

    get explanation() {
        return this._explanation;
    }

    get url() {
        return this._url;
    }

    get date() {
        return this._date;
    }

    get mediaType() {
        return this._mediaType;
    }

    get hdUrl() {
        return this._hdUrl;
    }

}