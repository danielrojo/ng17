export class ErrorApod {

    private _code = 0;
    private _message = '';
    private _serviceVersion = '';
    private _dateOutOfRange = false;

    constructor(error?: any) {
        if (!error) {
            return;
        }
        this._code = error.code;
        this._message = error.error.msg;
        this._serviceVersion = error.service_version;
        this._dateOutOfRange = this._message.includes('Date must be between Jun 16, 1995 and');
    }

    get code() {
        return this._code;
    }

    get message() {
        return this._message;
    }

    get serviceVersion() {
        return this._serviceVersion;
    }

    get dateOutOfRange() {
        return this._dateOutOfRange;
    }

    
}
    