import { Routes } from '@angular/router';
import { CalculatorComponent } from './components/calculator/calculator/calculator.component';
import { HeroesComponent } from './components/heroes/heroes/heroes.component';
import { sampleGuard } from './guards/sample.guard';

export const routes: Routes = [
    { path: 'calculator', component: CalculatorComponent },
  { path: 'heroes', component: HeroesComponent, canActivate: [sampleGuard] },
  { path: 'apod',  loadComponent: () => import('./components/apod/apod/apod.component').then(mod => mod.ApodComponent) },
  { path: 'beers', loadComponent: () => import('./components/beers/beers/beers.component').then(mod => mod.BeersComponent) },
  { path: 'forms', loadComponent: () => import('./components/forms/forms/forms.component').then(mod => mod.FormsComponent) },
  { path: 'starwars', loadComponent: () => import('./components/sw/sw/sw.component').then(mod => mod.SwComponent) },
  { path: '', redirectTo: '/calculator', pathMatch: 'full'},
  { path: '**', redirectTo: '/calculator', pathMatch: 'full'},

];
