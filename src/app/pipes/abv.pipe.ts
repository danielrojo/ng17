import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abv',
  standalone: true,
  pure: false,
})
export class AbvPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    let result = '';
    value < 0 || value > 100 ? result = 'N/A' : result = value.toFixed(1);   
    args.length > 0 ? result = result + args[0] : result = result + ' %';
    console.log('AbvPipe: ' + result);
     
    return result;
  }

}
