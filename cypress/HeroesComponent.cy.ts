import { provideHttpClient } from "@angular/common/http"
import { provideRouter } from "@angular/router"
import { routes } from "../src/app/app.routes"
import { HeroesComponent } from "../src/app/components/heroes/heroes/heroes.component"
import { Hero } from "../src/app/models/hero"
import { HeroesService } from "../src/app/services/heroes.service"

describe('HeroesComponent.cy.ts', () => {
  it('playground', () => {
    cy.mount(HeroesComponent, {
      providers: [
        HeroesService
      ]}) 
  })
})