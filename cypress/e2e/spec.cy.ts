describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:4200/forms');
    cy.get('#name').click();
    cy.get('#name').type('un nombre');
    cy.get('#description').click();
    cy.get('#description').type('una descripcion');
    cy.get('#name').should('have.value', 'un nombre');
    cy.get('#description').should('have.value', 'una descripcion');
    cy.get('#submit').click();
    cy.get('#name').should('have.value', '');
    cy.get('#description').should('have.value', '');
  })
})